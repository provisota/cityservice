package cityservice.model;

/**
 * @author Alterovych Ilya
 *         Date: 21.02.14
 */
public class CitiesModel {

    private final String city1;
    private final String city2;
    private final Integer distance;

    public CitiesModel(final String city1, final String city2,
                       final Integer distance) {
        this.city1 = city1;
        this.city2 = city2;
        this.distance = distance;
    }

    public final String getCity1() {
        return city1;
    }

    public final String getCity2() {
        return city2;
    }

    public final Integer getDistance() {
        return distance;
    }
}
