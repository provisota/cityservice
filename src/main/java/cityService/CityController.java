package cityservice;

import cityservice.dao.JdbcCitiesDao;
import cityservice.model.CitiesModel;
import cityservice.jsonmessages.Distance;
import cityservice.jsonmessages.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Alterovych Ilya
 *         Date: 21.02.14
 */
@Controller
public class CityController {
    @Autowired
    private JdbcCitiesDao jdbcCitiesDao;
    private CitiesModel resultCity;
//    private String template = "City1: %s<br>City2: %s<br>Distance: %s<br><br>";

    public CityController() {
    }

    @RequestMapping(value = "/getCity", method = RequestMethod.GET)
    @ResponseBody
    public final Distance getDistance(
            @RequestParam(value = "city1") final String city1,
            @RequestParam(value = "city2") final String city2) {
        try {
            resultCity = jdbcCitiesDao.getCities(city1, city2);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        return new Distance(resultCity.getDistance());
    }

    @RequestMapping(value = "/addCity", method = RequestMethod.GET)
    @ResponseBody
    public final Message addCity(
            @RequestParam(value = "city1") final String city1,
            @RequestParam(value = "city2") final String city2,
            @RequestParam(value = "dist") final String dist) {
        try {
            jdbcCitiesDao.addCities(city1, city2, dist);
        } catch (NumberFormatException e) {
            return new Message("Invalid distance input");
        }
        return new Message("Distance between cities added");
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public final List<CitiesModel> getAll() {
        return jdbcCitiesDao.getAll();
    }
}
