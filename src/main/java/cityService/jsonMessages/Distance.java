package cityservice.jsonmessages;

/**
 * @author Alterovych Ilya
 *         Date: 22.02.14
 */
public class Distance {
    private final Integer distance;

    public Distance(Integer distance) {
        this.distance = distance;
    }

    public final Integer getDistance() {
        return distance;
    }
}
