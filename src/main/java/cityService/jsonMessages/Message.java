package cityservice.jsonmessages;

/**
 * @author Alterovych Ilya
 *         Date: 22.02.14
 */
public class Message {
    private final String message;

    public Message(final String message) {
        this.message = message;
    }

    public final String getMessage() {
        return message;
    }
}
