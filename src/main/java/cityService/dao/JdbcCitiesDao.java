package cityservice.dao;

import cityservice.model.CitiesModel;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Alterovych Ilya
 *         Date: 21.02.14
 */
@Repository
public class JdbcCitiesDao implements CitiesDAO {
    private static JdbcTemplate jdbcTemplate;

    public JdbcCitiesDao() {
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.h2.Driver.class);
        dataSource.setUsername("sa");
        dataSource.setUrl("jdbc:h2:mem");
        dataSource.setPassword("");

        jdbcTemplate = new JdbcTemplate(dataSource);

        System.out.println("Creating tables");
        jdbcTemplate.execute("drop table cities if exists");
        jdbcTemplate.execute("create table cities(" +
                "id serial, city1 varchar(50) not null , " +
                "city2 varchar(50) not null , distance int(15) not null)");
    }

    @Override
    public final void addCities(final String city1, final String city2,
                                final String dist)
            throws NumberFormatException {
        Integer distance = Integer.parseInt(dist);
        String deleteSQL = "delete " +
                "from cities " +
                "WHERE city1 = ? AND city2 = ?";
        String updateSQL = "INSERT INTO cities (city1, city2, distance) " +
                "values (?,?,?)";
        jdbcTemplate.update(deleteSQL, new Object[]{city1, city2});
        jdbcTemplate.update(updateSQL, new Object[]{city1, city2, distance});
    }

    @Override
    public final CitiesModel getCities(final String city1, final String city2)
            throws EmptyResultDataAccessException {
        return jdbcTemplate.queryForObject("select * from cities " +
                "where (city1 = ? and city2 = ?) or " +
                "(city1 = ? and city2 = ?)", new Object[]{city1, city2, city2, city1},
                new RowMapper<CitiesModel>() {
                    @Override
                    public CitiesModel mapRow(final ResultSet rs, final int rowNum)
                            throws SQLException {
                        return new CitiesModel(rs.getString("city1"),
                                rs.getString("city2"),
                                rs.getInt("distance"));
                    }
                });
    }

    @Override
    public final List<CitiesModel> getAll() {
        return jdbcTemplate.query("select * from cities",
                new RowMapper<CitiesModel>() {
                    @Override
                    public CitiesModel mapRow(final ResultSet rs, final int rowNum)
                            throws SQLException {
                        return new CitiesModel(rs.getString("city1"),
                                rs.getString("city2"),
                                rs.getInt("distance"));
                    }
                });
    }
}
