package cityservice.dao;

import cityservice.model.CitiesModel;

import java.util.List;

/**
 * @author Alterovych Ilya
 *         Date: 21.02.14
 */
public interface CitiesDAO {
    void addCities(String city1, String city2, String distance);
    CitiesModel getCities(String city1, String city2);
    List<CitiesModel> getAll();
}
